#!/bin/bash

## STATS
# RAM
#ram=$(echo $(free --mega | grep Mem | awk '{print $3/$2 * 100.0}') | cut -d . -f 1)
ram=$(echo $(free --mega | grep Mem | awk '{print (($3 + $6) * 100) / $2}') | cut -d . -f 1)

# TEMP
temp=$(sensors -u | grep -A2 "temp.:" | grep "input" | tr -d [:blank:] | cut -d':' -f2 | cut -d'.' -f1)

## Time
time=$(date +%H:%M)

## Battery
battery_charge=$(upower -i $(upower -e | grep BAT) | egrep percentage | gawk '{print $2}')
battery_plug=$(upower -i $(upower -e | grep BAT) | egrep state | gawk '{print $2}')

if [ $battery_plug = "discharging" ]
then
    battery_icon='⚠'
else
    battery_icon='⚡'
fi

## Brightness
brightness_now=$(cat /sys/class/backlight/intel_backlight/brightness)
brightness_max=$(cat /sys/class/backlight/intel_backlight/max_brightness)
brightness=$(( ($brightness_now * 100) / $brightness_max ))

if [ $brightness -ne 0 ]
then
	brightness_icon='☀'
else
	brightness_icon='☼'
fi

## Audio
audio_volume=$(pacmd list-sinks | grep "volume" | head -n1 | cut -d: -f3 | cut -d/ -f2 | cut -d% -f1 | tr -d "[:space:]")

if [ $audio_volume -ne 0 ]
then
	audio_icon='🔊'
else
	audio_icon='🔇'
fi

## Network IP
network="$(ip -br -4 addr show enp4s0 | tr -s ' ' | cut -d ' ' -f 1): $(ip -br -4 addr show enp4s0 | tr -s ' ' | cut -d ' ' -f 3)\
  $(ip -br -4 addr show enp4s0 | tr -s ' ' | cut -d ' ' -f 2)"

echo "$ram% 🐏   |   $temp °C   |   $audio_volume% $audio_icon   |   $brightness% $brightness_icon   |   $battery_charge $battery_icon   |   $time 🕘   "
