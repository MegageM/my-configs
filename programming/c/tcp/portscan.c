#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <error.h>
#include <netdb.h>

#define STARTPORT 1
#define ENDPORT 65535

void
help ()
{
	printf ("scanner [ ehs ] IP\n"
			"-e\tend port\n"
			"-h\thelp\n"
			"-s\tstart port\n"
			"\n"
			"NOTA: de momento solo funciona con IPs, no con dominios\n");
	exit (0);
}


void
fail (const char *error)
{
	printf ("%s\n", error);
	exit (EXIT_FAILURE);
}


void
showports (int openports[])
{
	int i;
	for (i = 0; ; i++)
	{
		if (openports[ i ] == 0)
			break;

		printf ("\t:%d\n", openports[ i ]);
	}
}


int
getsock ()
{
	int sock;

	sock = socket (PF_INET, SOCK_STREAM, 0);
	if (sock < 0)
	{
		perror ("sock");
		exit (EXIT_FAILURE);
	}
	else
		return sock;
}


int
main (int argc, char *argv[]) 
{
	int sock, port, portcont;
	int startport = STARTPORT, endport = ENDPORT;
	int opt, index;
	char dest_ip[14];
	struct sockaddr_in dest_host;
	struct in_addr address;

	if (argc == 1)
		help();
	else
		if (argc == 2)
		{
			strcpy (dest_ip, argv[1]);
			//printf ("un parametro: %s\n", dest_ip);
		}
		else
		{
			while ((opt = getopt (argc, argv, "e:hs:")) != -1)
			{
				switch (opt)
				{
					case 'e': // End port
						endport = atoi (optarg);
						break;

					case 'h': // Help
						help();
						break;

					case 's': // Start port
						startport = atoi (optarg);
						break;
				}
			}
			if (optind == (argc-1))
			{
				//printf ("varios parametros\n");
				strcpy (dest_ip, argv[ optind ]);
			}
			else
				fail ("falta la ip");
		}

	int openports[ (endport - startport) + 1 ];

	// Open the socket
	sock = getsock();

	// Destination Address
	if ((inet_aton (dest_ip, &address)) == 0)
		fail ("Invalid IP");

	dest_host.sin_addr.s_addr = address.s_addr; 	// IPv4 address
	dest_host.sin_family = AF_INET; 				// Address Family

	for (port = startport, portcont = 0; port <= endport; port++)
	{
		dest_host.sin_port = htons (port);	// Port number
		printf ("port: %d\n", port);

		// Connect
		if ((connect (sock, (struct sockaddr *) &dest_host,
						sizeof (dest_host))) == 0)
		{
			printf ("%d: open\n", port);
			openports[ portcont ] = port;
			openports[ portcont + 1 ] = 0;
			portcont++;
		}
	}

	// Close
	close (sock);

	// Show open ports
	if (portcont == 0)
		printf ("No ports open\n");
	else
		showports (openports);
}
